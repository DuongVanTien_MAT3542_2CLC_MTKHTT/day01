<!DOCTYPE html>
<html>

<body>
    <div class="username-container">
        <div class="timezone">
            <?php
            // Đặt timezone là Việt Nam
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            // Hiển thị ngày/giờ hiện tại theo timezone Việt Nam
            echo 'Bây giờ là : ' . date('H:i');
            echo ', thứ ' . (date('N') + 1);
            echo ' ngày ' . date('d/m/Y');
            ?>
        </div>
        <form>

            <label for="username" class="label">Tên đăng nhập</label>
            <input type="text" id="username" name="username" class="input" required><br>
            <label for="password" class="label">Mật khẩu</label>
            <input type="password" class="input"><br>

            <label for="username" class="login">Đăng nhập</label>

        </form>
    </div>
</body>

<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <style>
        .username-container {
            border: 2px solid #0099ff;
            display: inline-block;
            margin: 10% 30%;
        }

        .timezone {
            border: 1px;
            margin: 20px 80px 10px;
            display: inline-block;
            background-color: #f0f0f0;
            width: 340px;
	    padding: 9px;
        }

        .label {
            display: inline-block;
            padding: 10px;
            border: 2px solid #02f;
            background-color: #0077cc;
            width: 150px;
            margin: 5px 10px 10px 80px;
            color: #fff;
        }

        .input {
            border: 2px solid;
            padding: 10px;
            color: #0099ff;
            font-weight: bold;
            outline: none;
            background-color: #fff;
            width: 150px;
        }

        .login {
            display: inline-block;
            padding: 15px 0px;
            border: 2px solid #04f;
            background-color: #0077cc;
            margin: 25px 0px 15px 185px;
            color: #fff;
            border-radius: 10px;
            width: 150px;
            text-align: center;
        }
    </style>
</head>

</html>