<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Thông tin sinh viên</title>
    <style>
        .student-info {
            border: 2px solid #0099cc;
            display: inline-block;
            margin: 5cm 14cm;
            width: 650px;
            padding: 20px;
        }

        .info-title {
            text-align: center;
            font-size: 20px;
            margin-top: 10px;
        }

        .info-label {
            display: inline-block;
            color: #000;
            width: 150px;
            font-weight: bold;
            margin: 10px;
        }

        .info-value {
            display: inline-block;
            color: #000;
            width: 300px;
            margin: 10px;
        }
    </style>
</head>

<body>
    <div class="student-info">
        <h1 class="info-title">Thông tin sinh viên đã đăng ký</h1>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["name"];
            $gender = $_POST["gender"];
            $birthdate = $_POST["ngay"] . "/" . $_POST["thang"] . "/" . $_POST["nam"];
            $address = $_POST["quan"] . ", " . $_POST["thanhpho"];
            $additionalInfo = $_POST["additional_info"];

            echo '<div class="info-label">Họ và tên:</div>';
            echo '<div class="info-value">' . $name . '</div>';

            echo '<div class="info-label">Giới tính:</div>';
            if ($gender == 0) {
                echo '<div class="info-value">Nam</div>';
            } else {
                echo '<div class="info-value">Nữ</div>';
            }

            echo '<div class="info-label">Ngày sinh:</div>';
            echo '<div class="info-value">' . $birthdate . '</div>';

            echo '<div class="info-label">Địa chỉ:</div>';
            echo '<div class="info-value">' . $address . '</div>';

            echo '<div class="info-label">Thông tin thêm:</div>';
            echo '<div class="info-value">' . $additionalInfo . '</div>';
        }
        ?>
    </div>
</body>

</html>
