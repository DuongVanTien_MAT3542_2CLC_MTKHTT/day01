<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Form</title>
    <style>
        .register {
            border: 2px solid #0099cc;
            display: inline-block;
            margin: 5cm 14cm;
            width: 650px;
            padding: 20px;
        }

        .form-title {
            text-align: center;
            font-size: 20px;
            margin-top: 10px;
        }

        .label_name,
        .label_gender,
        .label_birthdate,
        .label_additional_info,
        .label_address {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #32CD32;
            width: 100px;
            padding: 9px;
            margin: 10px;
            margin-left: 1cm;
        }


        .birthdate-select,
        .address-select {
            display: inline-flex;
            align-items: center;
            margin-top: 10px;
        }

        .input-birthdate,
        .input_name,
        .input-address,
        .select-box {
            border: 2px solid #0099cc;
            color: #000;
            width: 100px;
            padding: 10px;
            outline: none;
            margin: 0 10px;
        }

        .radio_gender {
            margin-right: 15px;
        }

        .select-box {
            width: 200px;
        }

        .label_additional_info {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #32CD32;
            width: 100px;
            padding: 9px;
            margin: 10px;
            margin-left: 1cm;
        }

        .textarea_additional_info {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 300px;
            padding: 10px;
            outline: none;
        }

        .error-message {
            color: red;
            text-align: left;
            margin-left: 1.3cm;
            margin-top: 10px;
            margin-bottom: 5px;
        }

        .required {
            color: red;
            margin-left: 4px;
        }

        .submit {
            border: 2px solid #0099cc;
            background-color: #32CD32;
            color: #fff;
            border-radius: 10px;
            padding: 10px;
            margin: 20px;
            width: 100px;
            display: block;
            margin: 0 auto;
        }
    </style>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function () {
            // District data
            var districtData = {
                "Hà Nội": ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
                "Tp. Hồ Chí Minh": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"],
            };

            // Function to update the districts based on the selected city
            $("#thanhpho").on("change", function () {
                var selectedCity = $(this).val();
                var $quanSelect = $("#quan");

                $quanSelect.empty();

                if (selectedCity in districtData) {
                    var districts = districtData[selectedCity];
                    for (var i = 0; i < districts.length; i++) {
                        $quanSelect.append($("<option>", {
                            value: districts[i],
                            text: districts[i]
                        }));
                    }
                } else {
                    $quanSelect.append($("<option>", {
                        value: "",
                        text: "--Chọn Quận--"
                    }));
                }
            });

            $("form").submit(function (event) {
                var errors = [];

                // Kiểm tra trường Họ và tên
                var name = $("#name").val();
                if (name.trim() === "") {
                    errors.push("Hãy nhập họ và tên.");
                }

                // Kiểm tra trường Giới tính
                var genderSelected = $("input[name='gender']:checked").length;
                if (genderSelected === 0) {
                    errors.push("Hãy chọn giới tính.");
                }

                // Kiểm tra trường Thành phố và Quận
                var selectedCity = $("#thanhpho").val();
                var selectedDistrict = $("#quan").val();
                if (selectedCity === "" || selectedDistrict === "") {
                    errors.push("Hãy chọn địa chỉ.");
                }

                // Xóa lỗi của biểu mẫu trước đó
                $(".error-message").empty();

                // Hiển thị thông báo lỗi
                if (errors.length > 0) {
                    event.preventDefault(); // Ngăn chặn việc gửi biểu mẫu nếu có lỗi
                    var errorHtml = "<div class='error-message'>";
                    for (var i = 0; i < errors.length; i++) {
                        errorHtml += errors[i] + "<br>";
                    }
                    errorHtml += "</div>";
                    $(".register").prepend(errorHtml);
                }
            });
        });
    </script>
</head>

<body>
    <div class="register">
        <h1 class="form-title">Form đăng ký sinh viên</h1>
        <form method="POST" action="register_student.php">
            <label for="name" class="label_name">Họ và tên<span class="required">*</span></label>
            <input type="text" id="name" name="name" class="input_name" required><br>

            <label class="label_gender">Giới tính<span class="required">*</span></label>
            <?php
            $genders = array(
                0 => 'Nam',
                1 => 'Nữ'
            );

            foreach ($genders as $genderValue => $genderText) {
                echo '<label>';
                echo '<input type="radio" name="gender" class="radio_gender" value="' . $genderValue . '">' . $genderText;
                echo '</label>';
            }
            ?><br>

            <label for="ngaysinh" class="label_birthdate">Ngày sinh<span class="required">*</span></label>
            <div class="birthdate-select">
                <select name="nam" id="nam" class="input-birthdate" required>
                    <option value="" selected disabled>--Năm--</option>
                    <?php
                    $currentYear = date("Y");
                    for ($i = $currentYear - 40; $i <= $currentYear - 15; $i++) {
                        echo "<option value='$i'>$i</option>";
                    }
                    ?>
                </select>

                <select name="thang" id="thang" class="input-birthdate" required>
                    <option value="" selected disabled>--Tháng--</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        $month = sprintf("%02d", $i);
                        echo "<option value='$month'>$month</option>";
                    }
                    ?>
                </select>
                <select name="ngay" id="ngay" class="input-birthdate" required>
                    <option value="" selected disabled>--Ngày--</option>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        $day = sprintf("%02d", $i);
                        echo "<option value='$day'>$day</option>";
                    }
                    ?>
                </select>
            </div>

            <label for="address" class="label_address">Địa chỉ<span class="required">*</span></label>
            <div class="address-select">
                <select name="thanhpho" id="thanhpho" class="input-address select-box" required>
                    <option value="" selected disabled>--Chọn Thành phố--</option>
                    <option value="Hà Nội">Hà Nội</option>
                    <option value="Tp. Hồ Chí Minh">Tp. Hồ Chí Minh</option>
                </select>

                <select name="quan" id="quan" class="input-address select-box" required>
                    <option value="" selected disabled>--Chọn Quận--</option>
                </select>
            </div>

            <label for="additional_info" class="label_additional_info">Thông tin thêm</label>
            <textarea id="additional_info" name="additional_info" class="textarea_additional_info"></textarea><br>

            <button class="submit" type="submit">Đăng ký</button>
        </form>
    </div>
</body>

</html>
