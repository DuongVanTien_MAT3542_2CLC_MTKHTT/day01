<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Danh sách sinh viên</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .student-list {
            margin: 50px 50px;
            background-color: #fff;
            border: 2px solid #ccc;
            border-radius: 10px;
            padding: 20px;
        }

        .label {
            margin: 5px 34px 5px 100px;
        }

        .label_tk {
            margin: 5px 15px 5px 100px;
        }

        .search {
            border: 2px solid blue;
            background-color: #4682B4;
            color: #fff;
            text-align: center;
            width: 120px;
            border-radius: 10px;
            padding: 9px 0px 10px;
            margin: 10px auto 4px 6cm;
            display: block;
            cursor: pointer;
        }

        #department {
            border: 2px solid #07f;
            outline: none;
            width: 183px;
            padding: 7px 0px 5px;
            background-color: #AFEEEE;
            margin: 0px 20px 20px 3px;
        }

        #tk {
            border: 2px solid #07f;
            outline: none;
            width: 180px;
            padding: 7px 0px 5px;
            background-color: #AFEEEE;
            margin: 0px 0px 7px 0px;
        }

        .button {
            width: 50px;
            border: 2px solid blue;
            background-color: #00BFFF;
            color: #fff;
            padding: 5px;
            cursor: pointer;
        }

        th {
            padding: 0px 10px 5px 10px;
            text-align: left;
        }

        td {
            padding: 3px 10px 5px 10px;
            text-align: left;
        }

        th.action,
        td.action {
            padding-left: 100px;
        }

        .add-students {
            display: flex;
            align-items: center;
        }

        .add {
            border: 2px solid blue;
            background-color: #4682B4;
            color: #fff;
            text-align: center;
            width: 90px;
            border-radius: 10px;
            padding: 10px 0px 10px;
            margin: 10px 0px 0px 258px;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <div class="student-list">
        <form method="get">
            <label for="department" class="label">Khoa</label>
            <select name="department" id="department">
                <option value=""></option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                );

                // Duyệt và hiển thị danh sách khoa học
                foreach ($departments as $key => $value) {
                    $selected = (isset($_GET['department']) && $_GET['department'] == $key) ? 'selected' : '';
                    echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                }
                ?>
            </select><br>

            <label for="tk" class="label_tk">Từ khóa</label>
            <input type="text" name="tk" id="tk" value="<?php echo isset($tk) ? $tk : ''; ?>">


            <button type="submit" class="search">Tìm kiếm</button>
        </form>
        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th class="action">Action</th>
            </tr>

            <?php
            $server = "localhost";
            $username = "root";
            $password = "";
            $database = "ltweb";

            // Kết nối đến cơ sở dữ liệu
            $conn = new mysqli($server, $username, $password, $database);

            if ($conn->connect_error) {
                die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
            }

            $department = isset($_GET['department']) ? $_GET['department'] : '';
            $keyword = isset($_GET['tk']) ? $_GET['tk'] : '';

            $sql = "SELECT * FROM students WHERE department LIKE '%$department%' AND (name LIKE '%$keyword%')";
            $result = mysqli_query($conn, $sql);

            if (!$result) {
                die("Lỗi truy vấn: " . mysqli_error($conn));
            }

            $count = 1;

            $num_rows = mysqli_num_rows($result);
            echo "<div class='add-students'>";
            echo "<p>Số sinh viên tìm thấy: " . $num_rows . "</p>";
            echo '<form action="register.php">
                    <button type="submit" class="add">Thêm</button>
                  </form>';
            echo "</div>";

            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $count . "</td>";
                echo "<td>" . $row['name'] . "</td>";
                echo "<td>" . $row['department'] . "</td>";
                echo '<td class="action"><button onclick="deleteStudent(' . $row['id'] . ')" class="button">Xóa</button> <button onclick="editStudent(' . $row['id'] . ')" class="button">Sửa</button></td>';
                echo "</tr>";
                $count++;
            }


            mysqli_close($conn);
            ?>
        </table>
    </div>

    <script>
        function deleteStudent(id) {
            if (confirm("Bạn có chắc chắn muốn xóa sinh viên này?")) {
                redirectTo("delete.php?id=" + id);
            }
        }

        function editStudent(id) {
            redirectTo("edit.php?id=" + id);
        }

        function redirectTo(url) {
            window.location.href = url;
        }
    </script>
</body>

</html>
