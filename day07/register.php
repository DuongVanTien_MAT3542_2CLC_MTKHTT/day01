<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #0099cc;
            display: inline-block;
            margin: 5cm 14cm;
            width: 450px;
            height: 450px;
        }

        .label_name,
        .label_department,
        .label_gender {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #32CD32;
            width: 100px;
            padding: 9px;
            margin: 10px;
            margin-left: 1cm;
        }

        .label_birthdate {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #fff;
                background-color: #32CD32;
                width: 100px;
                padding: 9px;
                margin: 10px ;
                margin-left: 1cm;
            }

            .input_birthdate {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #000;
                width: 137px;
                padding: 10px;
                outline: none;
            }

            .label_address {
                 display: inline-block;
                 border: 2px solid #0099cc;
                 color: #fff;
                 background-color: #32CD32;
                 width: 100px;
                 padding: 9px;
                 margin: 10px;
                 margin-left: 1cm;
                }

            .input_address {
                 display: inline-block;
                 border: 2px solid #0099cc;
                 color: #000;
                 padding: 10px 60px 10px 10px ;
                 margin: 10px;
                 margin-left: 0px;
                 margin-top: 5px;
                 outline: none;
                }

        .submit {
            border: 2px solid #0099cc;
            background-color: #32CD32;
            color: #fff;
            border-radius: 10px;
            padding: 10px;
            margin: 20px;
            width: 100px;
            display: block;
            margin: 0 auto;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 210px;
            padding: 10px;
            outline: none;
        }

        .radio_gender {
            margin: 8px;
        }

        .department {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 160px;
            padding: 10px;
	        outline: none;
        }

        .error-message {
               color: red;
               text-align: left;
               margin-left: 1.3cm;
               margin-top: 10px;
               margin-bottom: 5px;
        }
	    .required {
    		   color: red;
		       margin-left: 4px;
        }


    </style>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function () {
            $("#hinh_anh").change(function () {
                var selectedImage = $(this).val().split("\\").pop(); // Lấy tên tệp đã chọn
                $("#label_file").text(selectedImage);
            });

        $("form").submit(function (event) {
            var errors = [];

            // Kiểm tra trường Họ và tên
            var name = $("#name").val();
            if (name.trim() === "") {
                errors.push("Hãy nhập tên.");
            }

            // Kiểm tra trường Phân khoa
            var phankhoa = $("#phankhoa").val();
            if (phankhoa === "") {
                errors.push("Hãy chọn phân khoa.");
            }

            // Kiểm tra trường Ngày sinh
            var ngaysinh = $("#ngaysinh").val();
            if (ngaysinh.trim() === "") {
                errors.push("Hãy nhập ngày sinh.");
            } else if (/[^0-9/]/.test(ngaysinh)) {
                errors.push("Hãy nhập ngày sinh đúng định dạng.");
            } else {
                // Kiểm tra định dạng ngày sinh (dd/mm/yyyy)
                var dateParts = ngaysinh.split('/');
                if (dateParts.length === 3) {
                    var day = parseInt(dateParts[0], 10);
                    var month = parseInt(dateParts[1], 10);
                    var year = parseInt(dateParts[2], 10);

                    if (isNaN(day) || isNaN(month) || isNaN(year) ||
                        day < 1 || day > 31 || month < 1 || month > 12) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng.");
                    }
                }
            }

            // Xóa lỗi của biểu mẫu trước đó
            $(".error-message").empty();

            // Hiển thị thông báo lỗi
            if (errors.length > 0) {
                event.preventDefault(); // Ngăn chặn việc gửi biểu mẫu nếu có lỗi
                var errorHtml = "<div class='error-message'>";
                for (var i = 0; i < errors.length; i++) {
                    errorHtml += errors[i] + "<br>";
                }
                errorHtml += "</div>";
                $(".register").prepend(errorHtml);
            }
        });
    });
</script>



</head>

<body>
    <div class="register">
    <form id="registrationForm" method="POST" action="confirm.php" enctype="multipart/form-data">
            <label for="name" class="label_name">Họ và tên<span class="required">*</span></label>
            <input type="text" id="name" name="name" class="input_name"><br>

	<label class="label_gender">Giới tính<span class="required">*</span></label>
          <?php
            $genders = array(
                0 => 'Nam',
                1 => 'Nữ'
            );

            foreach ($genders as $genderValue => $genderText) {
                echo '<label>';
                echo '<input type="radio" name="gender" class="radio_gender" value="' . $genderValue . '">' . $genderText;
                echo '</label>';
            }
            ?><br>


            <label for="phankhoa" class="label_department">Phân khoa<span class="required">*</span></label>
            <select name="phankhoa" id="phankhoa" class="department">
                <option value="" selected>--Chọn phân khoa--</option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu'
                );

                foreach ($departments as $key => $value) {
                    echo '<option value="' . $key . '">' . $value . '</option>';
                }
                ?>
            </select><br>

            <label for="ngaysinh" class="label_birthdate">Ngày sinh<span class="required">*</span></label>
            <input type="text" id="ngaysinh" name="ngaysinh" class="input_birthdate" placeholder="dd/mm/yyyy"><br>

            <label for="diachi" class="label_address">Địa chỉ</label>
            <input type="text" id="diachi" name="diachi" class="input_address"><br>

            <label for="" class="label_address">Hình ảnh</label>
            <input type="file" name="imageUpload" id="imageUpload"><br>


            <button class="submit" type="submit">Đăng ký</button>
        </form>
    </div>
</body>



</html>