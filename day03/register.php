<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <style>
        .register {
            border: 2px solid #0099cc;
            display: inline-block;
            margin: 5cm 14cm;
width: 450px;
height: 228px;
        }

        .label_name,
        .label_department,
        .label_gender {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #0099cc;
            width: 100px;
            padding: 9px;
            margin: 10px;
        }

        .submit {
            border: 2px solid #0099cc;
            background-color: #00cc66;
            border-radius: 10px;
            padding: 10px;
            margin: 20px;
            width: 100px;
            display: block;
            margin: 0 auto;
        }

        .input_name {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 200px;
            padding: 10px;
            outline: none;
        }


        .department {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 160px;
            padding: 10px;
	    outline: none;
        }
    </style>
</head>

<body>
    <div class="register">

        <form>
            <label for="name" class="label_name">Họ và tên</label>
            <input type="text" id="name" class="input_name"><br>
            <label class="label_gender">Giới tính</label>
            <input type="radio" id="male" name="gender" value="0">
            <label for="male">Nam</label>
            <input type="radio" id="female" name="gender" value="1">
            <label for="female">Nữ</label><br>
            <label for="phankhoa" class="label_department">Phân khoa</label>
            <select name="phankhoa" id="phankhoa" class="department">
                <option value="" selected>--Chọn phân khoa--</option>
                <?php
                $departments = ["Khoa học máy tính", "Khoa học vật liệu"];
                foreach ($departments as $department) {
                    echo "<option value='$department'>$department</option>";
                }
                ?>
            </select><br>
            <button class="submit">Đăng ký</button>
        </form>
    </div>
</body>

</html>