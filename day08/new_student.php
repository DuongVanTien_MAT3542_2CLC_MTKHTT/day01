<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Danh sách sinh viên</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        .student-list {
            margin: 50px 50px;
            background-color: #fff;
            border: 2px solid #ccc;
            border-radius: 10px;
            padding: 20px;
        }

        .label {
            margin-top: 5px;
            margin-right: 34px;
            margin-bottom: 5px;
            margin-left: 100px;
        }

        .label_tk {
            margin-top: 5px;
            margin-right: 15px;
            margin-bottom: 5px;
            margin-left: 100px;
        }

        .reset {
            border: 2px solid blue;
            background-color: #4682B4;
            color: #fff;
            text-align: center;
            width: 120px;
            border-radius: 10px;
            padding: 9px 0px 10px;
            margin: 10px auto 4px 6cm;
            display: block;
            cursor: pointer;
        }

        #department {
            border: 2px solid #07f;
            outline: none;
            width: 183px;
            padding: 7px 0px 5px;
            background-color: #AFEEEE;
            margin: 0px 20px 20px 3px;
        }

        #phankhoa {
            border: 2px solid #07f;
            outline: none;
            width: 183px;
            padding: 7px 0px 5px;
            background-color: #AFEEEE;
            margin: 0px 20px 20px 3px;
        }

        #tk {
            border: 2px solid #07f;
            outline: none;
            width: 180px;
            padding: 7px 0px 5px;
            background-color: #AFEEEE;
            margin: 0px 0px 7px 0px;
        }

        .add-students {
            display: flex;
            align-items: center;
        }

        .add {
            border: 2px solid blue;
            background-color: #4682B4;
            color: #fff;
            text-align: center;
            width: 90px;
            border-radius: 10px;
            padding: 10px 0px 10px;
            margin: 10px 0px 0px 258px;
            cursor: pointer;
        }

        .button {
            width: 50px;
            border: 2px solid blue;
            background-color: #00BFFF;
            color: #fff;
            padding: 5px;
            cursor: pointer;
        }

        th {
            padding: 0px 10px 5px 10px;
            text-align: left;
        }

        td {
            padding: 3px 10px 5px 10px;
            text-align: left;
        }

        th.action,
        td.action {
            padding-left: 100px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#resetButton").click(function () {
                $("#searchForm")[0].reset();
                $("#studentTableBody").html("");
                $(".add-students").hide();
            });

            $("#phankhoa").change(function () {
                var department = $(this).val();
                var keyword = $("#tk").val();

                if (keyword === "") {
                    $.ajax({
                        url: "search.php",
                        type: "GET",
                        data: { department: department },
                        success: function (response) {
                            $("#studentTableBody").html(response);
                            $(".add-students").show();
                            $("#numStudents").text($("#studentTableBody tr").length);
                        },
                        error: function (xhr, status, error) {
                            alert("Lỗi: " + error);
                        }
                    });
                } else {
                    $("#studentTableBody").html("");
                    $(".add-students").hide();
                    $("#numStudents").text("");
                }
            });

            $("#tk").on("keyup", function () {
                var department = $("#phankhoa").val();
                var keyword = $(this).val();

                $.ajax({
                    url: "search.php",
                    type: "GET",
                    data: { department: department, keyword: keyword },
                    success: function (response) {
                        $("#studentTableBody").html(response);
                        keyword ? $(".add-students").hide() : $(".add-students").show();
                        $("#numStudents").text($("#studentTableBody tr").length - 1);
                    },
                    error: function (xhr, status, error) {
                        alert("Lỗi: " + error);
                    }
                });
            });
        });
    </script>
</head>

<body>
    <div class="student-list">
        <form method="get" id="searchForm">
            <label for="phankhoa" class="label">Khoa</label>
            <select name="phankhoa" id="phankhoa">
                <option value=""></option>
                <?php
                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                );

                foreach ($departments as $key => $value) {
                    $selected = (isset($_GET['phankhoa']) && $_GET['phankhoa'] == $key) ? 'selected' : '';
                    echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                }
                ?>
            </select><br>

            <label for="tk" class="label_tk">Từ khóa</label>
            <input type="text" name="tk" id="tk" value="<?php echo isset($tk) ? $tk : ''; ?>">

            <button type="button" class="reset" id="resetButton">Reset</button>
        </form>
        <div class="add-students">
            <p>Số sinh viên tìm thấy: <span id="numStudents"></span></p>
            <form action="register.php">
                <button type="submit" class="add">Thêm</button>
            </form>
        </div>
        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th class="action">Action</th>
            </tr>
            <tbody id="studentTableBody">
                <!-- Hiển thị kết quả tìm kiếm sinh viên tại đây -->
            </tbody>
        </table>
    </div>
</body>

</html>
