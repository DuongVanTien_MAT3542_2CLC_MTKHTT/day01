<?php
$server = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

// Kết nối đến cơ sở dữ liệu
$conn = new mysqli($server, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Xử lý yêu cầu xóa sinh viên
if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["id"])) {
    $deleteId = $_GET["id"];

    // Xóa sinh viên từ cơ sở dữ liệu
    $deleteQuery = "DELETE FROM students WHERE id = $deleteId";

    if ($conn->query($deleteQuery) === TRUE) {
        echo "success";
    } else {
        echo "error";
    }
}

// Đóng kết nối cơ sở dữ liệu
$conn->close();
?>
