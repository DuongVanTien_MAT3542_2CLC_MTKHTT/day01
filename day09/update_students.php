<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Cập nhật sinh viên</title>
    <style>
        /* Sao chép CSS từ register.php */
        .register {
            border: 2px solid #0099cc;
            display: inline-block;
            margin: 5cm 14cm;
        }

        .label_name,
        .label_department,
        .label_gender {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #32CD32;
            width: 100px;
            padding: 9px;
            margin: 4px;
            margin-left: 1cm;
        }

        .label_birthdate {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #32CD32;
            width: 100px;
            padding: 9px;
            margin: 4px;
            margin-left: 1cm;
        }

        .input_birthdate {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 137px;
            padding: 10px;
            outline: none;
        }

        .label_address {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #fff;
            background-color: #32CD32;
            width: 100px;
            padding: 9px;
            margin: 4px;
            margin-left: 1cm;

        }

        .input_address {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            padding: 10px 60px 10px 10px;
            margin: 10px;
            margin-left: 0px;
            margin-top: 5px;
            outline: none;
        }

        .submit {
            border: 2px solid #0099cc;
            background-color: #32CD32;
            color: #fff;
            border-radius: 10px;
            padding: 10px;
            margin: 20px 200px 20px;
            width: 100px;
            display: block;

        }

        .input_name {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 210px;
            padding: 10px;
            outline: none;
        }

        .radio_gender {
            margin: 8px;
        }

        .department {
            display: inline-block;
            border: 2px solid #0099cc;
            color: #000;
            width: 160px;
            padding: 10px;
            outline: none;
        }

        .error-message {
            color: red;
            text-align: left;
            margin-left: 1.3cm;
            margin-top: 10px;
            margin-bottom: 5px;
        }

        .required {
            color: red;
            margin-left: 4px;
        }

        .label_file {
            display: none;
        }

        .hinh_anh {
            display: inline;
            max-width: 120px;
            max-height: 120px;
            vertical-align: top;
            margin: 4px;
        }
    </style>
</head>

<body>
    <div class="register">
        <?php
        require_once 'database.php'; // Kết nối đến cơ sở dữ liệu

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $student_id = $_POST["student_id"];
            $name = $_POST["name"];
            $gender = $_POST["gender"];
            $phankhoa = $_POST["phankhoa"];
            $ngaysinh = $_POST["ngaysinh"];
            $diachi = $_POST["diachi"];

            // Chuyển đổi ngày tháng về định dạng phù hợp (Y-m-d)
            $ngaysinh = date('Y-m-d', strtotime(str_replace('/', '-', $ngaysinh)));

            // Câu truy vấn SQL để cập nhật thông tin của sinh viên
            $update_query = "UPDATE students SET
                             name = '$name',
                             gender = '$gender',
                             department = '$phankhoa',
                             birthdate = '$ngaysinh',
                             address = '$diachi'
                             WHERE id = '$student_id'";

            if ($conn->query($update_query) === TRUE) {
                echo "Thông tin sinh viên đã được cập nhật thành công.";
            } else {
                echo "Lỗi: " . $update_query . "<br>" . $conn->error;
            }
        }

        // Lấy giá trị id từ URL
        $student_id = $_GET['id'];

        // Truy vấn cơ sở dữ liệu để lấy thông tin sinh viên
        $sql = "SELECT * FROM students WHERE id = '$student_id'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $name = $row['name'];
            $gender = $row['gender'];
            $phankhoa = $row['department'];
            $ngaysinh = $row['birthdate'];
            $diachi = $row['address'];

            $ngaysinh_display = date('d/m/Y', strtotime($ngaysinh));
            ?>
            <form method="POST" action="">
                <input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
                <label for="name" class="label_name">Họ và tên<span class="required">*</span></label>
                <input type="text" id="name" name="name" class="input_name" value="<?php echo $name; ?>"><br>

                <label class="label_gender">Giới tính<span class="required">*</span></label>
                <?php
                $genders = array(
                    0 => 'Nam',
                    1 => 'Nữ'
                );

                foreach ($genders as $genderValue => $genderText) {
                    $checked = ($gender == $genderValue) ? 'checked' : '';
                    echo '<label>';
                    echo '<input type="radio" name="gender" class="radio_gender" value="' . $genderValue . '" ' . $checked . '>' . $genderText;
                    echo '</label>';
                }
                ?><br>

                <label for="phankhoa" class="label_department">Phân khoa<span class="required">*</span></label>
                <select name="phankhoa" id="phankhoa" class="department">
                    <option value="" selected>--Chọn phân khoa--</option>
                    <?php
                    $departments = array(
                        'MAT' => 'Khoa học máy tính',
                        'KDL' => 'Khoa học vật liệu'
                    );

                    foreach ($departments as $key => $value) {
                        $selected = ($phankhoa == $key) ? 'selected' : '';
                        echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                    }
                    ?>
                </select><br>

                <label for="ngaysinh" class="label_birthdate">Ngày sinh<span class="required">*</span></label>
                <input type="text" id="ngaysinh" name="ngaysinh" class="input_birthdate" value="<?php echo $ngaysinh_display; ?>"><br>

                <label for="diachi" class="label_address">Địa chỉ</label>
                <input type="text" id="diachi" name="diachi" class="input_address" value="<?php echo $diachi; ?>"><br>

                <input type="submit" name="submit" value="Xác nhận" class="submit">
            </form>
            <?php
        } else {
            echo "<p class='error-message'>Không tìm thấy sinh viên.</p>";
        }
        ?>
    </div>
</body>
</html>