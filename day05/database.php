<?php
$server = "localhost";  // Địa chỉ máy chủ MySQL (thường là localhost)
$username = "root";      // Tên đăng nhập mặc định cho XAMPP là "root"
$password = "";          // Mật khẩu mặc định là trống
$database = "ltweb";    // Tên cơ sở dữ liệu

// Tạo kết nối đến cơ sở dữ liệu
$conn = new mysqli($server, $username, $password, $database);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Đặt bộ mã hóa kết nối (tuỳ thuộc vào nhu cầu của bạn)
$conn->set_charset("utf8");
?>
