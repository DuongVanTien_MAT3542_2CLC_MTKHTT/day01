<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Confirmation</title>
    <style>
            /* Sao chép CSS từ register.php */
            .register {
                border: 2px solid #0099cc;
                display: inline-block;
                margin: 5cm 14cm;
            }

            .label_name,
            .label_department,
            .label_gender {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #fff;
                background-color: #32CD32;
                width: 100px;
                padding: 9px;
                margin: 4px;
                margin-left: 1cm;
            }

            .label_birthdate {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #fff;
                background-color: #32CD32;
                width: 100px;
                padding: 9px;
                margin: 4px;
                margin-left: 1cm;
            }

            .input_birthdate {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #000;
                width: 137px;
                padding: 10px;
                outline: none;
            }

            .label_address {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #fff;
                background-color: #32CD32;
                width: 100px;
                padding: 9px;
                margin: 4px;
                margin-left: 1cm;

            }

            .input_address {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #000;
                padding: 10px 60px 10px 10px;
                margin: 10px;
                margin-left: 0px;
                margin-top: 5px;
                outline: none;
            }

            .submit {
                border: 2px solid #0099cc;
                background-color: #32CD32;
                color: #fff;
                border-radius: 10px;
                padding: 10px;
                margin: 20px 200px 20px;
                width: 100px;
                display: block;

            }

            .input_name {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #000;
                width: 210px;
                padding: 10px;
                outline: none;
            }

            .radio_gender {
                margin: 8px;
            }

            .department {
                display: inline-block;
                border: 2px solid #0099cc;
                color: #000;
                width: 160px;
                padding: 10px;
                outline: none;
            }

            .error-message {
                color: red;
                text-align: left;
                margin-left: 1.3cm;
                margin-top: 10px;
                margin-bottom: 5px;
            }

            .required {
                color: red;
                margin-left: 4px;
            }

            .label_file {
                display: none;
            }

            .hinh_anh {
                display: inline;
                max-width: 120px;
                max-height: 120px;
                vertical-align: top;
                margin: 4px;
            }
        </style>
</head>

<body>
    <div class="register">
        <?php
        require_once 'database.php'; // Kết nối đến cơ sở dữ liệu

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = $_POST["name"];
                $gender = $_POST["gender"];
                $phankhoa = $_POST["phankhoa"];
                $ngaysinh = $_POST["ngaysinh"];
                $diachi = $_POST["diachi"];

                // Xử lý hình ảnh nếu có
                $image = null;
                if (isset($_FILES['imageUpload'])) {
                    $file = $_FILES['imageUpload'];
                    if ($file['error'] === UPLOAD_ERR_OK) {
                        $file_data = file_get_contents($file['tmp_name']);
                        $base64_image = base64_encode($file_data);
                        $image = $base64_image;
                    }
                }

                $genders = array(
                    0 => 'Nam',
                    1 => 'Nữ'
                );

                $departments = array(
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu'
                );

                echo "<p><span class='label_name'>Họ và tên</span> $name</p>";
                echo "<p><span class='label_gender'>Giới tính</span> " . $genders[$gender] . "</p>";
                echo "<p><span class='label_department'>Phân khoa</span> " . $departments[$phankhoa] . "</p>";
                echo "<p><span class='label_birthdate'>Ngày sinh</span> $ngaysinh</p>";
                echo "<p><span class='label_address'>Địa chỉ</span> $diachi</p>";

                // Hiển thị hình ảnh nếu có
                if (!empty($image)) {
                    echo "<p><span class='label_address'>Hình ảnh</span><img src='data:image/jpeg;base64,$image' class='hinh_anh'></p>";
                }
            }

        ?>
                <form method="POST" action="database.php">
                    <input type="hidden" name="name" value="<?php echo $name; ?>">
                    <input type="hidden" name="gender" value="<?php echo $gender; ?>">
                    <input type="hidden" name="phankhoa" value="<?php echo $phankhoa; ?>">
                    <input type="hidden" name="ngaysinh" value="<?php echo $ngaysinh; ?>">
                    <input type="hidden" name="diachi" value="<?php echo $diachi; ?>">
                    <input type="submit" name="submit" value="Xác nhận" class="submit">
                </form>
            </div>
        </body>
        </html>