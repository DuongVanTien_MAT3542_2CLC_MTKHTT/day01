<?php
$server = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

// Kết nối đến cơ sở dữ liệu
$conn = new mysqli($server, $username, $password, $database);

if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

if (isset($_POST["submit"])) { // Kiểm tra nút "Xác nhận" đã được nhấn
    if (isset($_POST["name"]) && isset($_POST["gender"]) && isset($_POST["phankhoa"]) && isset($_POST["ngaysinh"]) && isset($_POST["diachi"])) {
        // Lấy giá trị từ mảng $_POST
        $name = $_POST["name"];
        $gender = $_POST["gender"];
        $phankhoa = $_POST["phankhoa"];
        $ngaysinh = $_POST["ngaysinh"];
        $diachi = $_POST["diachi"];

        // Kiểm tra và xử lý hình ảnh (nếu có)
        $imageName = null;
        if (isset($_FILES['imageUpload'])) {
            $file = $_FILES['imageUpload'];
            if ($file['error'] === UPLOAD_ERR_OK) {
                $imageName = $file['name']; // Lấy tên tệp ảnh
            }
        }

        if ($gender === '0') {
            $gender = 'Nam';
        } elseif ($gender === '1') {
            $gender = 'Nữ';
        }

        // Chuyển đổi giá trị "phankhoa" thành "Khoa học máy tính" hoặc "Khoa học vật liệu"
        if ($phankhoa === 'MAT') {
            $phankhoa = 'Khoa học máy tính';
        } elseif ($phankhoa === 'KDL') {
            $phankhoa = 'Khoa học vật liệu';
        }

        // Chuẩn bị và thực hiện truy vấn SQL để chèn dữ liệu
        $sql = "INSERT INTO students (name, gender, department, birthdate, address, image)
                VALUES ('$name', '$gender', '$phankhoa', STR_TO_DATE('$ngaysinh', '%d/%m/%Y'), '$diachi', ?)";

        // Sử dụng prepared statement để lưu hình ảnh vào cơ sở dữ liệu
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("b", $imageName);

        if ($stmt->execute()) {
            echo "Dữ liệu đã được lưu vào cơ sở dữ liệu.";
        } else {
            echo "Lỗi." . $conn->error;
        }

        $stmt->close();
    }
}

// Đóng kết nối cơ sở dữ liệu
$conn->close();
?>
